package constants;


import java.util.Scanner;


public class Constant {

    // ----SUB-MODEL-METHODS--
    public static final String NAME = "Input name for ";
    public static final String SURNAME = "Input surname for ";
    public static final String CREATED = "There are created rows \n";
    public static final String DELETED = "There are deleted rows \n";
    // ----GENERAL-CONSTANTS--
    public static final Scanner INPUT = new Scanner(System.in);
    //-----DEPENDENCIES--
    public static final String URL_KEY = "url";
    public static final String USER_KEY = "user";
    public static final String PASSWORD_KEY = "password";

}
