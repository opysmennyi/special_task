package model.meta;

public class ColumnMeta {
    private String columnName;
    private String dataType;
    private String columnSize;
    private boolean isNullable;
    private boolean isAutoIncrement;
    private boolean isPrimaryKey;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setColumnSize(String columnSize) {
        this.columnSize = columnSize;
    }

    public void setNullable(boolean nullable) {
        isNullable = nullable;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        isAutoIncrement = autoIncrement;
    }

    public void setPrimaryKey(boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    @Override
    public String toString() {
        String str = String.format("%-15s  %-12s  %-15s  %s  %s",
                columnName,
                dataType + "(" + columnSize + ")",
                (isNullable ? "NULL" : "NOT NULL"),
                (isPrimaryKey ? "PK" : ""),
                (isAutoIncrement ? "  AutoIncrement" : ""));
        return str;
    }
}
