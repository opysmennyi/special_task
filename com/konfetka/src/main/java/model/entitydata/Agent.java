package model.entitydata;

import model.annotation.Column;
import model.annotation.Table;

@Table(name = "agent")
public class Agent {
    @Column(name = "id_agent")
    private int id_agent;
    @Column(name = "personal_info_passport_id")
    private String personal_info_passport_id;

    public Agent() {
    }

    public Agent(int id_agent, String personal_info_passport_id) {
        this.id_agent = id_agent;
        this.personal_info_passport_id = personal_info_passport_id;
    }

    public int getId_agent() {
        return id_agent;
    }

    public void setId_agent(int id_agent) {
        this.id_agent = id_agent;
    }

    public String getPersonal_info_passport_id() {
        return personal_info_passport_id;
    }

    public void setPersonal_info_passport_id(String personal_info_passport_id) {
        this.personal_info_passport_id = personal_info_passport_id;
    }

    @Override
    public String toString() {
        return String.format("%d %-11s", getId_agent(), getPersonal_info_passport_id());
    }
}
