package model.entitydata;

import model.annotation.Column;
import model.annotation.Table;

@Table(name="regestry_number")
public class RegestryNumber {
    @Column(name="regestry_number")
    private String regestry_number;

    public RegestryNumber() {
    }

    public RegestryNumber(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    public String getRegestry_number() {
        return regestry_number;
    }

    public void setRegestry_number(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    @Override
    public String toString() {
        return String.format("%-11s", getRegestry_number());
    }
}
