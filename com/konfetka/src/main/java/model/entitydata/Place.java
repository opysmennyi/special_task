package model.entitydata;

import model.annotation.Column;
import model.annotation.Table;

@Table(name = "place")
public class Place {
    @Column(name = "id_place")
    private int id_place;
    @Column(name = "square")
    private int square;
    @Column(name = "price")
    private int price;
    @Column(name = "location")
    private String location;
    @Column(name = "type")
    private String type;
    @Column(name = "id_landor_id")
    private int landor_id;
    @Column(name = "regestry_number_regestry_number")
    private String regestry_number;

    public Place() {
    }

    public Place(int id_place, int square, int price, String location, String type, int landor_id, String regestry_number) {
        this.id_place = id_place;
        this.square = square;
        this.price = price;
        this.location = location;
        this.type = type;
        this.landor_id = landor_id;
        this.regestry_number = regestry_number;
    }

    public int getId_place() {
        return id_place;
    }

    public void setId_place(int id_place) {
        this.id_place = id_place;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLandor_id() {
        return landor_id;
    }

    public void setLandor_id(int landor_id) {
        this.landor_id = landor_id;
    }

    public String getRegestry_number() {
        return regestry_number;
    }

    public void setRegestry_number(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    @Override
    public String toString() {
        return String.format("%d %d %d %-11s %-11s %d %-11s",
                getId_place(),
                getSquare(),
                getPrice(),
                getLocation(),
                getType(),
                getLandor_id(),
                getRegestry_number());
    }
}
