package model.entitydata;

import model.annotation.Column;
import model.annotation.Table;

@Table(name = "agency")
public class Agency {
    @Column(name = "id_agency")
    private int id_agency;
    @Column(name = "name")
    private String name;
    @Column(name = "city")
    private String city;
    @Column(name = "payment_terms")
    private String payment_ters;
    @Column(name = "markup")
    private int markup;
    @Column(name = "agency_services_price")
    private int agency_services_price;
    @Column(name = "agreement_termination")
    private String agreement_termination;

    public Agency() {
    }

    public Agency(int id_agency, String name, String city, String payment_ters, int markup, int agency_services_price, String agreement_termination) {
        this.id_agency = id_agency;
        this.name = name;
        this.city = city;
        this.payment_ters = payment_ters;
        this.markup = markup;
        this.agency_services_price = agency_services_price;
        this.agreement_termination = agreement_termination;
    }

    public int getId_agency() {
        return id_agency;
    }

    public void setId_agency(int id_agency) {
        this.id_agency = id_agency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPayment_ters() {
        return payment_ters;
    }

    public void setPayment_ters(String payment_ters) {
        this.payment_ters = payment_ters;
    }

    public int getMarkup() {
        return markup;
    }

    public void setMarkup(int markup) {
        this.markup = markup;
    }

    public int getAgency_services_price() {
        return agency_services_price;
    }

    public void setAgency_services_price(int agency_services_price) {
        this.agency_services_price = agency_services_price;
    }

    public String getAgreement_termination() {
        return agreement_termination;
    }

    public void setAgreement_termination(String agreement_termination) {
        this.agreement_termination = agreement_termination;
    }

    @Override
    public String toString() {
        return String.format("%d %-11s %-11s %-11s %d %d %-11s",
                getId_agency(),
                getName(),
                getCity(),
                getPayment_ters(),
                getMarkup(),
                getAgency_services_price(),
                getAgreement_termination());
    }
}
