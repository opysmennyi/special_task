package model.entitydata;

import model.annotation.Column;
import model.annotation.Table;

@Table(name = "landor")
public class Landor {
    @Column(name = "id_landor")
    private int id_landor;
    @Column(name = "personal_info_passport_id")
    private String personal_info_passport_id;

    public Landor() {
    }

    public Landor(int id_landor, String personal_info_passport_id) {
        this.id_landor = id_landor;
        this.personal_info_passport_id = personal_info_passport_id;
    }

    public int getId_landor() {
        return id_landor;
    }

    public void setId_landor(int id_landor) {
        this.id_landor = id_landor;
    }

    public String getPersonal_info_passport_id() {
        return personal_info_passport_id;
    }

    public void setPersonal_info_passport_id(String personal_info_passport_id) {
        this.personal_info_passport_id = personal_info_passport_id;
    }

    @Override
    public String toString() {
        return String.format("%d %-11s", getId_landor(), getPersonal_info_passport_id());
    }
}
