package view_controller.view_submodel;

import constants.Constant;
import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class UpdateMethods {
    private static Logger LOG = LogManager.getLogger(UpdateMethods.class);

    public static void updateSupplier() throws SQLException {
        LOG.trace("Input ID(supplier id) for Supplier: ");
        Integer id_supplier = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input name for Supplier: ");
        String name = Constant.INPUT.next();
        LOG.trace("Input surname for Supplier: ");
        String surname = Constant.INPUT.next();
        LOG.trace("Input flower quantiy for Supplier: ");
        Integer flower_quantity_sup = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Supplier supplier = new Supplier(id_supplier, name, surname, flower_quantity_sup);
        SupplierService supplierService = new SupplierService();
        int count = supplierService.update(supplier);
        LOG.info(Constant.CREATED, count);
    }

    public static void updateFlowerShop() throws SQLException {
        LOG.trace("Input Flower Shop ID for Shop: ");
        Integer id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input emp_fmane for Employee: ");
        String name = Constant.INPUT.nextLine();
        LOG.trace("Input emp_lname for Employee: ");
        Integer flower_quantity_shop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Flowers_shop flowers_shop = new Flowers_shop(id_fshop, name, flower_quantity_shop);
        FlowersShopService flowersShopService = new FlowersShopService();
        int count = flowersShopService.update(flowers_shop);
        LOG.info(Constant.CREATED, count);
    }

    public static void updateOrder() throws SQLException {
        LOG.trace("Input Order ID for order: ");
        Integer id_order = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input flower quantity for order: ");
        String order_quantity = Constant.INPUT.nextLine();
        LOG.trace("Input supplier_id for order: ");
        Integer supplier_id = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input flower_shop_id for order: ");
        Integer flower_shop_id = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Order order = new Order(id_order, order_quantity, supplier_id, flower_shop_id);
        OrderService orderService = new OrderService();
        int count = orderService.update(order);
        LOG.info(Constant.CREATED, count);
    }

    public static void updateOwner() throws SQLException {
        LOG.trace("Input Owner ID for owner: ");
        Integer id_owner = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input Owner's name for owner: ");
        String name_owner = Constant.INPUT.nextLine();
        LOG.trace("Input Owner's surname for owner: ");
        String surname_owner = Constant.INPUT.nextLine();
        Owner owner = new Owner(id_owner, name_owner, surname_owner);
        OwnerService ownerService = new OwnerService();
        int count = ownerService.update(owner);
        LOG.info(Constant.CREATED, count);
    }

    public static void updateFowerShopHasOwner() throws SQLException {
        LOG.trace("Input Owner ID for FowerShopHasOwner: ");
        String id_owner = Constant.INPUT.nextLine();
        LOG.trace("Input flower_shop_id_fshop's ID for flower_shop_id_fshop: ");
        Integer flower_shop_id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Flower_shop_has_owner flower_shop_has_owner = new Flower_shop_has_owner(id_owner, flower_shop_id_fshop);
        FlowerShopHasOwnerServise flowerShopHasOwnerServise = new FlowerShopHasOwnerServise();
        int count = flowerShopHasOwnerServise.update(flower_shop_has_owner);
        LOG.info(Constant.CREATED, count);
    }
}
