package view_controller.view_submodel;

import constants.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view_controller.MyViewMenu;
import view_controller.MyViewMethodsMenu;

public class MenuMethods {
    public static Logger LOG = LogManager.getLogger(MyViewMenu.class);
    MyViewMenu myView = new MyViewMenu();
    MyViewMethodsMenu myViewMethodsMenu = new MyViewMethodsMenu();

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            LOG.trace("Please, select menu point.");
            keyMenu = Constant.INPUT.nextLine().toUpperCase();
            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                LOG.trace("Please, select menu point.");
                keyMenu = Constant.INPUT.nextLine().toUpperCase();
            }
            try {
                myViewMethodsMenu.methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        LOG.trace("\nMENU:");
        for (String key : myView.menu.keySet())
            if (key.length() == 1) {
                LOG.trace(myView.menu.get(key));
            }
    }

    private void outputSubMenu(String fig) {
        LOG.info("\nSubMENU:");
        for (String key : myView.menu.keySet()) {
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) {
                LOG.trace(myView.menu.get(key));
            }
        }
    }
}
