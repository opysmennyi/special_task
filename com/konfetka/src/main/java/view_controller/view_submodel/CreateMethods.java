package view_controller.view_submodel;

import constants.Constant;
import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class CreateMethods {

    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

    public static void createSupplier() throws SQLException {
        LOG.trace("Input ID(supplier id) for Supplier: ");
        Integer id_supplier = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace(Constant.NAME + Constant.SUPPLIER);
        String name = Constant.INPUT.nextLine();
        LOG.trace(Constant.SURNAME + Constant.SUPPLIER);
        String surname = Constant.INPUT.nextLine();
        LOG.trace("Input flower quantiy for " + Constant.SUPPLIER);
        Integer flower_quantity_sup = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Supplier supplier = new Supplier(id_supplier, name, surname, flower_quantity_sup);

        SupplierService supplierService = new SupplierService();
        int count = supplierService.create(supplier);
        LOG.info(Constant.CREATED + count);
    }

    public static void createFlowerShop() throws SQLException {
        LOG.trace("Input Flower Shop ID for Shop: ");
        Integer id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace(Constant.NAME + "Flower Shop: ");
        String name = Constant.INPUT.nextLine();
        LOG.trace("Input flower quantity for Shop: ");
        Integer flower_quantity_shop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Flowers_shop flowers_shop = new Flowers_shop(id_fshop, name, flower_quantity_shop);
        FlowersShopService flowersShopService = new FlowersShopService();
        int count = flowersShopService.create(flowers_shop);
        LOG.info(Constant.CREATED + count);
    }

    public static void createOrder() throws SQLException {
        LOG.trace("Input Order ID for order: ");
        Integer id_order = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input order_quantity for order: ");
        String order_quantity = Constant.INPUT.nextLine();
        LOG.trace("Input supplier_id ID for order: ");
        Integer supplier_id = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input supplier_id ID for order: ");
        Integer flower_shop_id = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        Order order = new Order(id_order, order_quantity, supplier_id, flower_shop_id);

        OrderService orderService = new OrderService();
        int count = orderService.create(order);
        LOG.info(Constant.CREATED + count);
    }

    public static void createFlowerShopHasOwner() throws SQLException {
        LOG.trace("Input Flower shop ID for FShop: ");
        Integer flower_shop_id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace(Constant.NAME + "for FShop: ");
        String owner_id = Constant.INPUT.nextLine();
        Flower_shop_has_owner flowerShopHasOwner = new Flower_shop_has_owner(owner_id, flower_shop_id_fshop);
        FlowerShopHasOwnerServise flowerShopHasOwnerServise = new FlowerShopHasOwnerServise();
        int count = flowerShopHasOwnerServise.create(flowerShopHasOwner);
        LOG.info(Constant.CREATED + count);
    }

    public static void createOwner() throws SQLException {
        LOG.trace("Input Owner ID for owner: ");
        Integer Id_owner = Constant.INPUT.nextInt();
        LOG.trace(Constant.NAME + "for owner: ");
        String name_owner = Constant.INPUT.nextLine();
        LOG.trace(Constant.SURNAME + "surname for owner: ");
        String surname_owner = Constant.INPUT.nextLine();

        Owner owner = new Owner(Id_owner, name_owner, surname_owner);
        OwnerService ownerService = new OwnerService();
        int count = ownerService.create(owner);
        LOG.info(Constant.CREATED + count);
    }
}
