package view_controller.view_submodel;

import constants.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.FlowersShopService;
import service.implementation.OrderService;
import service.implementation.OwnerService;
import service.implementation.SupplierService;

import java.sql.SQLException;

public class DeleteMethods {
    private static Logger LOG = LogManager.getLogger(DeleteMethods.class);

    public static void deleteFromSupplier() throws SQLException {
        LOG.trace("Input ID(supplier id) for Suppllier: ");
        Integer id_supplier = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        SupplierService supplierService = new SupplierService();
        int count = supplierService.delete(id_supplier);
        LOG.info(Constant.DELETED, count);
    }

    public static void deleteFlowerShop() throws SQLException {
        LOG.trace("Input Shop ID for Shop: ");
        Integer id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        FlowersShopService flowersShopService = new FlowersShopService();
        int count = flowersShopService.delete(id_fshop);
        LOG.info(Constant.DELETED, count);
    }

    public static void deleteOrder() throws SQLException {
        LOG.trace("Input Order ID for order: ");
        Integer id_order = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        OrderService orderService = new OrderService();
        int count = orderService.delete(id_order);
        LOG.info(Constant.DELETED, count);
    }

    public static void deleteOwner() throws SQLException {
        LOG.trace("Input Owner ID for owner: ");
        Integer id_owner = Constant.INPUT.nextInt();
        OwnerService ownerService = new OwnerService();
        int count = ownerService.delete(id_owner);
        LOG.info(Constant.DELETED, count);
    }

    public static void deleteFlowerShopHasOwner() throws SQLException {
        LOG.trace("Input flower_shop_has_owner ID for flower_shop_has_owner: ");
        Integer o_id_owner = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        FlowersShopService flowersShopService = new FlowersShopService();
        int count = flowersShopService.delete(o_id_owner);
        LOG.info(Constant.DELETED, count);
    }

    public static void deleteFromSupplierMoveToShop() throws SQLException {
        LOG.trace("Input ID(supplier id) for this Supplier: ");
        Integer idDeleted = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        LOG.trace("Input ID(dept_no) for another Department (for move employees):");
        Integer idMoveTo = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();

        SupplierService supplierService = new SupplierService();
        int count = supplierService.deleteWithMoveSupplier(idDeleted, idMoveTo);
        LOG.info(Constant.DELETED, count);
    }
}
