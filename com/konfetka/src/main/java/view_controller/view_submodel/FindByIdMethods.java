package view_controller.view_submodel;

import constants.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.FlowersShopService;
import service.implementation.OrderService;
import service.implementation.OwnerService;
import service.implementation.SupplierService;

import java.sql.SQLException;

public class FindByIdMethods {
    private static Logger LOG = LogManager.getLogger(FindByIdMethods.class);

    public static void findSupplierByID() throws SQLException {
        LOG.trace("Input ID(supplier id) for Supplier: ");
        Integer id_supplier = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        SupplierService supplierService = new SupplierService();
        Supplier supplier = supplierService.findById(id_supplier);
        LOG.trace(supplier);
    }

    public static void findFlowerShopByID() throws SQLException {
        LOG.trace("Input Flower Shop ID for Shop: ");
        Integer id_fshop = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        FlowersShopService flowersShopService1 = new FlowersShopService();
        Flowers_shop flowers_shop = flowersShopService1.findById(id_fshop);
        LOG.trace(flowers_shop);
    }

    public static void findOrderByID() throws SQLException {
        LOG.trace("Input order ID for order: ");
        Integer id_order = Constant.INPUT.nextInt();
        Constant.INPUT.nextLine();
        OrderService orderService = new OrderService();
        Order order = orderService.findById(id_order);
        LOG.trace(order);
    }

    public static void findOwnerByID() throws SQLException {
        LOG.trace("Input owner ID for Owner: ");
        Integer id_order = Constant.INPUT.nextInt();
        OwnerService ownerService = new OwnerService();
        Owner owner = ownerService.findById(id_order);
        LOG.trace(owner);
    }
}
