package view_controller;

import view_controller.view_submodel.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyViewMethodsMenu {
    public Map<String, Printable> methodsMenu;

    public MyViewMethodsMenu() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("A", SelectMethods::selectAllTable);
        methodsMenu.put("B", TakeStructureOfDB::takeStructureOfDB);
        methodsMenu.put("11", CreateMethods::createSupplier);
        methodsMenu.put("12", UpdateMethods::updateSupplier);
        methodsMenu.put("13", DeleteMethods::deleteFromSupplier);
        methodsMenu.put("14", SelectMethods::selectSupplier);
        methodsMenu.put("15", FindByIdMethods::findSupplierByID);
        methodsMenu.put("16", DeleteMethods::deleteFromSupplierMoveToShop);
        methodsMenu.put("21", CreateMethods::createFlowerShop);
        methodsMenu.put("22", UpdateMethods::updateFlowerShop);
        methodsMenu.put("23", DeleteMethods::deleteFlowerShop);
        methodsMenu.put("24", SelectMethods::selectFlowerShop);
        methodsMenu.put("25", FindByIdMethods::findFlowerShopByID);
        methodsMenu.put("31", CreateMethods::createOrder);
        methodsMenu.put("32", UpdateMethods::updateOrder);
        methodsMenu.put("33", DeleteMethods::deleteOrder);
        methodsMenu.put("34", SelectMethods::selectOrder);
        methodsMenu.put("35", FindByIdMethods::findOrderByID);
        methodsMenu.put("41", CreateMethods::createOwner);
        methodsMenu.put("42", UpdateMethods::updateOwner);
        methodsMenu.put("43", DeleteMethods::deleteOwner);
        methodsMenu.put("44", SelectMethods::selectOwner);
        methodsMenu.put("45", FindByIdMethods::findOwnerByID);
        methodsMenu.put("51", CreateMethods::createFlowerShopHasOwner);
        methodsMenu.put("52", UpdateMethods::updateFowerShopHasOwner);
        methodsMenu.put("53", DeleteMethods::deleteFlowerShopHasOwner);
        methodsMenu.put("54", SelectMethods::selectFowerShopHasOwner);

    }
}