package view_controller;

import constants.Constant;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyViewMenu {
    public Map<String, String> menu;

    public MyViewMenu() {
        menu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");
        menu.put("1", "   1 - Table: " + Constant.SUPPLIER);
        menu.put("11", "  11 - " + Constant.CREATE + Constant.SUPPLIER);
        menu.put("12", "  12 - " + Constant.UPDATE + Constant.SUPPLIER);
        menu.put("13", "  13 - " + Constant.DELETE + Constant.SUPPLIER);
        menu.put("14", "  14 - " + Constant.SELECT + Constant.SUPPLIER);
        menu.put("15", "  15 - " + Constant.FINDBYID + Constant.SUPPLIER);
        menu.put("16", "  16 - Delete from supplier and move all flowers to shop");
        menu.put("2", "   2 - Table: " + Constant.FLOWERSHOP);
        menu.put("21", "  21 - " + Constant.CREATE + Constant.FLOWERSHOP);
        menu.put("22", "  22 - " + Constant.UPDATE + Constant.FLOWERSHOP);
        menu.put("23", "  23 - " + Constant.DELETE + Constant.FLOWERSHOP);
        menu.put("24", "  24 - " + Constant.SELECT + Constant.FLOWERSHOP);
        menu.put("25", "  25 - " + Constant.FINDBYID + Constant.FLOWERSHOP);
        menu.put("3", "   3 - Table: " + Constant.ORDER);
        menu.put("31", "  31 - " + Constant.CREATE + Constant.ORDER);
        menu.put("32", "  32 - " + Constant.UPDATE + Constant.ORDER);
        menu.put("33", "  33 - " + Constant.DELETE + Constant.ORDER);
        menu.put("34", "  34 - " + Constant.SELECT + Constant.ORDER);
        menu.put("35", "  35 - " + Constant.FINDBYID + Constant.ORDER);
        menu.put("4", "   4 - Table: " + Constant.OWNER);
        menu.put("41", "  41 - " + Constant.CREATE + Constant.OWNER);
        menu.put("42", "  42 - " + Constant.UPDATE + Constant.OWNER);
        menu.put("43", "  43 - " + Constant.DELETE + Constant.OWNER);
        menu.put("44", "  44 - " + Constant.SELECT + Constant.OWNER);
        menu.put("45", "  45 - " + Constant.FINDBYID + Constant.OWNER);
        menu.put("5", "   5 - Table: " + Constant.FLOWERSHOPHASOWNER);
        menu.put("51", "  51 - " + Constant.CREATE + Constant.FLOWERSHOPHASOWNER);
        menu.put("52", "  52 - " + Constant.UPDATE + Constant.FLOWERSHOPHASOWNER);
        menu.put("53", "  53 - " + Constant.DELETE + Constant.FLOWERSHOPHASOWNER);
        menu.put("54", "  54 - " + Constant.SELECT + Constant.FLOWERSHOPHASOWNER);
        menu.put("Q", "   Q - exit");
    }
}
